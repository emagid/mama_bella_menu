<div class="form-group">
    <label>Show only from this office:</label>
    <select class="filter-offices form-control">
        <option value="">All Patients</option>
        <? foreach($model->offices as $office){ ?>
            <option value="<?=$office->O?>" <?=$office->O == $model->office_id ? 'selected' : ''?> ><?=$office->OFN?></option>
        <? }?>
    </select>
</div>
<?php
 if(count($model->patients)>0): ?>
  <div class="box box-table">
    <table class="table">
      <thead>
        <tr>
           <th width="30%">Patient Name</th>
           <th width="5%">Sex</th>
           <th width="20%">Office</th>
           <th width="10%">Last Visit</th>
           <th width="10%">Email</th>
           <th width="10%">Home Phone</th>
           <th width="10%">Cell Phone</th>
        </tr>
      </thead>
      <tbody>
       <?php foreach($model->patients as $obj){ ?>
       <tr>
        <td>
           <a href="<?php echo ADMIN_URL; ?>patients/update/<?=$obj->PID?>">
             <?="{$obj->LN}, {$obj->FN}".(!empty($obj->MI) ? ' '.$obj->MI : '' )?>
           </a>
        </td>
        <td>
         <a href="<?php echo ADMIN_URL; ?>patients/update/<?=$obj->PID?>">
           <?=$obj->SX;?>
         </a>
        </td>
        <td>
          <a href="<?php echo ADMIN_URL; ?>patients/update/<?=$obj->PID?>">
             <span class="office-name" data-office="<?=$obj->O?>"><?=$obj->O?></span>
          </a>
        </td>
        <td>
            <a href="<?php echo ADMIN_URL; ?>patients/update/<?=$obj->PID?>">
                <?=date('m/d/y',strtotime($obj->LV))?>
            </a>
        </td>
        <td>
            <a href="<?php echo ADMIN_URL; ?>patients/update/<?=$obj->PID?>">
                <?=$obj->EM?>
            </a>
        </td>
        <td>
            <a href="<?php echo ADMIN_URL; ?>patients/update/<?=$obj->PID?>">
                <?=$obj->HP?>
            </a>
        </td>
        <td>
            <a href="<?php echo ADMIN_URL; ?>patients/update/<?=$obj->PID?>">
                <?=$obj->CP?>
            </a>
        </td>
       </tr>
       <?php } ?>
   </tbody>
 </table>
 <div class="box-footer clearfix">
  <div class='paginationContent'></div>
</div>
</div>
<?php endif; ?>

<?php echo footer(); ?>
<script type="text/javascript">
    var site_url = '<?= ADMIN_URL.'patients';?><?=false ? "?office={$model->office_id}&" :'' ?>';
    var total_pages = <?= $model->pagination['total_pages'];?>;
    var page = <?= $model->pagination['current_page_index'];?>;

    $(document).ready(function(){
       $('select.filter-offices').on('change',function(){
           window.location.href = window.location.origin + window.location.pathname + '?office='+this.value;
       });
    });
</script>