<?php

namespace Notification;

class ErrorHandler extends \Notification\NotificationsHandler
{
  /**
   * 
   * Construct the error object
   * 
   */
  public function __construct($messages)
  {
  	if (is_array($messages)){
  		$message = '<ul>';
  		foreach($messages as $m){
  			$message .= '<li>';
  			$message .= $m['message'];
  			$message .= '</li>';
  		}
  		$message .= '</ul>';
  	} else {
  		$message = $messages;
  	}
    $this->message = $message;
    $this->buildHTML();
  }
  
  /**
   * 
   * Display the error
   * 
   */
  protected function buildHTML()
  {
    $this->html .= "<div class='alert alert-danger'>";
    $this->html .= "<strong>An error occurred: </strong>";
    $this->html .= $this->message;
    $this->html .= "</div>";
  }
  
}
