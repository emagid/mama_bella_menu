<?php

namespace Model;

class Order_Product extends \Emagid\Core\Model {
    static $tablename = "order_products";
  
    public static $fields = [
  		'order_id',
  		'product_map_id',
  		'quantity',
  		'unit_price',
    ];

	public function ring(){
		return Order_Product_Option::getItem(null, ['where' => "order_product_id = $this->id"]);
	}

	public function product()
	{
		$map = Product_Map::getItem($this->product_map_id);
		return $map->product();
	}

	public function totalPrice()
	{
		$total = $this->unit_price;
		if(!($option = $this->ring())){
			return $total;
		}

		return $total + $option->price;
	}

	// Should only get one option row, put everything into json datatype
	public function getOptions()
	{
		return Order_Product_Option::getItem(null, ['where' => "order_product_id = {$this->id}"]);
//		return Order_Product_Option::getList(['where' => "order_product_id = {$this->id}"]);
	}
}